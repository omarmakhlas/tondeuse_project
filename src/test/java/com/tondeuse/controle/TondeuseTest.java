package com.tondeuse.controle;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * @author Omar
 *
 */
public class TondeuseTest {

	Tondeuse tondeuse  = new Tondeuse();
	
	/**
	 * @throws FileNotFoundException
	 * @throws IllegalPointException 
	 * throw IllegalPointException parceque l'une des valeur du coin supérieur de la pelouse esty négatif
	 */
	@Test(expected = IllegalPointException.class)
	public void testTondeuse_cointSuperiereInvalid_IllegalPointException() throws FileNotFoundException, IllegalPointException {
		String position_final = tondeuse.parcourir(new File("src/test/resources/inputs/invalide-input.txt"));
	}
	
	
	/**
	 * @throws FileNotFoundException
	 * @throws IllegalPointException
	 * La position initial est en dehors de la pelosue
	 */
	@Test(expected = IllegalPointException.class)
	public void testTondeuse_positionInitialInvalid_IllegalPointException() throws FileNotFoundException, IllegalPointException {
		String position_final = tondeuse.parcourir(new File("src/test/resources/inputs/position-initial-invalid.txt"));
	}
	
	
	/**
	 * @throws FileNotFoundException
	 * @throws IllegalPointException
	 * return une seule position final
	 */
	@Test
	public void testTondeuse_positionInitialValid_positionFinal() throws FileNotFoundException, IllegalPointException {
		String position_final = tondeuse.parcourir(new File("src/test/resources/inputs/input.txt"));
		assertEquals("1 3 E", position_final);
	}
	
	/**
	 * @throws FileNotFoundException
	 * @throws IllegalPointException
	 * return 2 positions finales
	 */
	@Test
	public void testTondeuse_positionInitialValid_multiplePositionFinal() throws FileNotFoundException, IllegalPointException {
		String position_final = tondeuse.parcourir(new File("src/test/resources/inputs/multiple-input.txt"));
		assertEquals("1 3 N\n5 1 E", position_final);
	}
	
	
	/**
	 * @throws FileNotFoundException
	 * @throws IllegalPointException
	 * return la position finale mais en dehors de la pelouse, la tondeuse ne bouge pas et traite la commande suivante
	 */
	@Test
	public void testTondeuse_positionInitialValid_DehorsPelouse() throws FileNotFoundException, IllegalPointException {
		String position_final = tondeuse.parcourir(new File("src/test/resources/inputs/dehors-pelouse.txt"));
		assertEquals("1 5 S", position_final);
	}
	
	
	
	

}
