﻿package com.tondeuse.controle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Omar
 *
 */
public class Tondeuse {
	/**
	 * @param file
	 * @return la position final des tondeuses
	 * @throws FileNotFoundException
	 */
	public String parcourir(File file) throws FileNotFoundException, IllegalPointException {

		String resultat = "";
		
		Map<Character, Integer> orientation = new HashMap<>();
		orientation.put('N', 0);
		orientation.put('E', 1);
		orientation.put('S', 2);
		orientation.put('W', 3);
		
		// Le tablaru de carateres G contient l'orientation après avoir pivoter la tondeuse à gauche
		Character[] G = {'W', 'N', 'E', 'S'};
		
		// Le tablaru de carateres G contient l'orientation après avoir pivoter la tondeuse à droite
		Character[] D = {'E', 'S', 'W', 'N'};
 
		Map<Character, Point> avance = new HashMap<Character, Point>();
		avance.put('N', new Point(0, 1));
		avance.put('E', new Point(1, 0));
		avance.put('S', new Point(0, -1));
		avance.put('W', new Point(-1, 0));
 
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(file);
		int x_pos = 0;
		int y_pos = 0;
		char orientation_x_y = 0;
		int x_max = sc.nextInt();
		int y_max = sc.nextInt();
		if(x_max<0 || y_max<0) throw new IllegalPointException(x_max, y_max);
		while(sc.hasNext()) {
 
			x_pos = sc.nextInt();
			y_pos = sc.nextInt();
			if(x_pos>x_max || y_pos > y_max || x_pos<0 || y_pos<0) throw new IllegalPointException(x_pos, y_pos);
			orientation_x_y = sc.next().charAt(0);
 
			String instruction = sc.next();
			for(char c : instruction.toCharArray()) {
				if(c == 'G') {
					char new_orentation = G[orientation.get(orientation_x_y)];
					orientation_x_y = new_orentation;
				}
				else if(c == 'D') {
					char new_orentation = D[orientation.get(orientation_x_y)];
					orientation_x_y = new_orentation;
				}
				else if(c == 'A') {
					if(x_pos == x_max && avance.get(orientation_x_y).getX() == 1 || (x_pos == 0 && avance.get(orientation_x_y).getX() == -1))
						continue;
					else if(y_pos == y_max && avance.get(orientation_x_y).getY() == 1 || (y_pos == 0 && avance.get(orientation_x_y).getY() == -1))
						continue;
					else {
						x_pos += avance.get(orientation_x_y).getX();
						y_pos += avance.get(orientation_x_y).getY();
					}
				}
			}
			System.out.println( ""+x_pos+" "+y_pos+" "+orientation_x_y);
			resultat = resultat + x_pos+" "+y_pos+" "+orientation_x_y;
			if(sc.hasNext()) resultat = resultat +"\n";
		}
		return resultat;
	}
	

}
