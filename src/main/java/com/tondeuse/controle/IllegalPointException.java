package com.tondeuse.controle;


/**
 * @author Omar
 *
 */
public class IllegalPointException extends Exception {
    
    private static final long serialVersionUID = -9204191749972551939L;
    
	private int x;
	private int y;
    
    /**
     * @param illegalx
     * @param illegaly
     */
    public IllegalPointException(int illegalx, int illegaly) {
        x = illegalx;
        x = illegaly;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Throwable#toString()
     */
    public String toString() {
        return "Illegal point : " + x+", "+y;
    }
}
