# Tondeuse_project

C'est un projet maven bas� sur JUnit et TDD
Les cas des testes mis en place sont :
	1 - L'une des valeur du coin sup�rieur est n�gative => Exception
	2 - La position initial est en dehors de la pelouse => Exception
	3 - les 3 testes qui restent sont valides avec des exemples differents

Pour la logique de cet exercise :
	1 - J'ai cr�� une map avec l'orientation suivante : (N,0), (E,1)->(S,2)->(W,3)
	    les valeurs me permettent d'avoir la nouvelle position apr�s orientation � Gauche ou � droite 
	    par indice des tableaux suivants : G = {'W', 'N', 'E', 'S'} et D = {'E', 'S', 'W', 'N'};

	2 - Ensuite j'ai cr�� une map pour le cas ou on l'avance la tondeuse dans la direction � laquelle elle fait face


